# Tree-Sitter Bindings for Gambit-C (Scheme)

These bindings can be loaded at runtime or compiled into an executable.
In order to successfully build the bindings, one has to make sure that
the tree-sitter (shared) library is on the `$LD_LIBRARY_PATH`.

It is used in my language server, which can be referred to at:
[zamba language server](https://gitlab.com/tomaha.gq/zamba-ls)

Within this repository, you can see for usage of these bindings.
To make use of them, one needs to build bindings to your preferred parsers as well.

# LICENSE

MIT License
Copyright (c) 2021 Thomas Hage
