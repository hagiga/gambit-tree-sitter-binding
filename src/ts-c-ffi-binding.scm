;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; this is the tree-sitter module to load into Gambit-C
;; it only binds to the api.h header

;; FIXME: types, structs, unions should have a tags field
;;        there is no need for a release function yet.

(c-declare #<<C-END

#include <tree_sitter/api.h>
#include <string.h>

extern char * read_buffer_parse_cb(TSPoint position);

const char * read_buffer_and_release_string(void *payload, uint32_t byte_index, TSPoint position, uint32_t *bytes_read)
{
  char * str = read_buffer_parse_cb(position);
  *bytes_read = strlen(str);
  return str;
  #define ___AT_END if (str != NULL) ___release_rc(str);
}

static TSInput ip = (TSInput) { NULL, &read_buffer_and_release_string, 0 };

C-END
)

(c-define-type ts--field-id unsigned-int16)
(c-define-type ts--symbol unsigned-int16)

(c-define-type ts--language (struct "TSLanguage"))
(c-define-type ts--parser (struct "TSParser"))
(c-define-type ts--tree (struct "TSTree"))

(c-define-type ts--node "TSNode")
(c-define-type ts--input "TSInput")
(c-define-type ts--input-edit "TSInputEdit")
(c-define-type ts--range "TSRange")
(c-define-type ts--symbol-type unsigned-int16) ; enum
(c-define-type ts--point "TSPoint")

(c-define-type ts--cursor "TSTreeCursor")

(c-define-type ts--query-predicate-step-type unsigned-int16)
(c-define-type ts--query-predicate-step "TSQueryPredicateStep")
(c-define-type ts--query (struct "TSQuery"))
(c-define-type ts--query-cursor (struct "TSQueryCursor"))

(c-define-type ts--query-error "TSQueryError")
(c-define-type ts--query-match "TSQueryMatch")
(c-define-type ts--query-capture "TSQueryCapture")

(c-define-type ts--input-encoding unsigned-int16)

(define make--ts-input-edit                        (c-lambda (unsigned-int32 unsigned-int32 unsigned-int32 (pointer ts--point) (pointer ts--point) (pointer ts--point))
                                                             (pointer ts--input-edit)
                                                             "TSInputEdit * edit = (TSInputEdit *) malloc(sizeof(TSInputEdit));
                                                             edit->start_byte = ___arg1;
                                                             edit->old_end_byte = ___arg2;
                                                             edit->new_end_byte = ___arg3;
                                                             edit->start_point = *___arg4;
                                                             edit->old_end_point = *___arg5;
                                                             edit->new_end_point = *___arg6;
                                                             ___result = (TSInputEdit *) edit;"))

(define (ts-col->col line col)
  (letrec ((walk-string
             (lambda (p col-p)
               (if (= col-p col)
                   p
                   (let ((c (char->integer (string-ref line p))))
                     (cond
                       ((< c     #x80) (walk-string (+ p 1) (+ col-p 1)))
                       ((< c    #x800) (walk-string (+ p 1) (+ col-p 2)))
                       ((< c  #x10000) (walk-string (+ p 1) (+ col-p 3)))
                       ((< c #x110000) (walk-string (+ p 1) (+ col-p 4)))
                       (else (raise "invalid character"))))))))
    (walk-string 0 0)))


(define ts--parser-current-payload #f)

(c-define (parser-read-func point) (ts--point) nonnull-UTF-8-string "read_buffer_parse_cb" "extern"
          (let ((content ts--parser-current-payload)
                (row (ts--point-row point))
                (col (ts--point-col point)))
            (cond
              ((< row (vector-length content))
               (let ((line (vector-ref content row)))
                 (cond
                   ((= col 0) line)
                   (else
                     (substring line (ts-col->col line col) (string-length line))))))
              (else
                ""))))

(define ts--range-start-point                      (c-lambda ((pointer ts--range) unsigned-int32) ts--point "___return((TSPoint) ___arg1[___arg2].start_point);"))
(define ts--range-end-point                        (c-lambda ((pointer ts--range) unsigned-int32) ts--point "___return((TSPoint) ___arg1[___arg2].end_point);"))
(define ts--range-start-byte                       (c-lambda ((pointer ts--range) unsigned-int32) unsigned-int32 "___result = ___arg1[___arg2].start_byte;"))
(define ts--range-end-byte                         (c-lambda ((pointer ts--range) unsigned-int32) unsigned-int32 "___result = ___arg1[___arg2].end_byte;"))
(define ts--range-free!                            (c-lambda ((pointer ts--range)) void "free"))

(define make--ts-point                             (c-lambda (unsigned-int32 unsigned-int32) (pointer ts--point) "TSPoint* p = (TSPoint *) malloc(sizeof(TSPoint)); p->row = ___arg1; p->column = ___arg2; ___result = p;"))
(define ts--point-row                              (c-lambda ((pointer ts--point)) unsigned-int32 "___result = ___arg1->row;"))
(define ts--point-col                              (c-lambda ((pointer ts--point)) unsigned-int32 "___result = ___arg1->column;"))

(define ts--predicate-type                         (c-lambda ((pointer ts--query-predicate-step)) ts--query-predicate-step-type "___result = ___arg1->type;"))
(define ts--predicate-value                        (c-lambda ((pointer ts--query-predicate-step)) unsigned-int32 "___result = ___arg1->value_id;"))

(define make-ts--query-error                       (c-lambda () (pointer ts--query-error) "___result = (TSQueryError *) malloc(sizeof(TSQueryError));"))
(define ts--query-error-get                        (c-lambda ((pointer ts--query-error)) unsigned-int32 "___result = *___arg1;"))

(define make-ts--query-match                       (c-lambda () (pointer ts--query-match) "___return((TSQueryMatch *) malloc(sizeof(TSQueryMatch)));"))

;; incompatible with u32vector..
(define make-unsigned-int32*                       (c-lambda () (pointer unsigned-int32) "uint32_t * init = malloc(sizeof(uint32_t)); * init = 0; ___result = init;"))
(define ts--resolve-uint-32                        (c-lambda ((pointer unsigned-int32)) unsigned-int32 "___result = *___arg1;"))
(define unsigned-int32-set!                        (c-lambda ((pointer unsigned-int32) int) void "*___arg1 = ___arg2;"))
(define unsigned-int32-free!                       (c-lambda ((pointer unsigned-int32)) void "free"))

;; index of the matches / match-count
(define ts--query-match-id                         (c-lambda ((pointer ts--query-match)) unsigned-int32 "___result = ___arg1->id;"))
;; the index of the pattern the capture/match resides in
(define ts--query-match-pattern-index              (c-lambda ((pointer ts--query-match)) unsigned-int16 "___result = ___arg1->pattern_index;"))
;; length of capture-array
(define ts--query-match-capture-count              (c-lambda ((pointer ts--query-match)) unsigned-int16 "___result = ___arg1->capture_count;"))
(define ts--query-match-capture                    (c-lambda ((pointer ts--query-match)) (pointer ts--query-capture) "___result = (TSQueryCapture *) ___arg1->captures;"))
(define ts--query-match-capture-next!              (c-lambda ((pointer ts--query-match)) void "___arg1->captures++;"))
(define ts--query-match-capture-node-at            (c-lambda ((pointer ts--query-match) unsigned-int32) (pointer ts--query-capture) "___result = (TSQueryCapture *) &___arg1->captures[___arg2];"))

;; the captured node
(define ts--query-capture-node                     (c-lambda ((pointer ts--query-capture)) ts--node "___return(___arg1->node);"))
;; the index of all captures provided (@see ts--query-capture-name-for-id)
;; the order can be printed by using:
;;   grep query.scm "@.*" | awk '!x[$0]++'
(define ts--query-capture-index                    (c-lambda ((pointer ts--query-capture)) unsigned-int32 "___result = ___arg1->index;"))

(define ts--parser-new                             (c-lambda () (pointer ts--parser) "ts_parser_new"))
(define ts--parser-free!                           (c-lambda ((pointer ts--parser)) void "ts_parser_delete"))
(define ts--parser-language-set!                   (c-lambda ((pointer ts--parser) (pointer ts--language)) bool "ts_parser_set_language"))
(define ts--parser-lang?                           (c-lambda ((pointer ts--parser)) (pointer ts--language) "___return((TSLanguage *)ts_parser_language);"))
(define ts--parser-included-ranges-set!            (c-lambda ((pointer ts--parser) (pointer ts--range) unsigned-int32) bool "ts_parser_set_included_ranges"))
(define ts--parser-included-ranges?                (c-lambda ((pointer ts--parser) (pointer unsigned-int32)) (pointer ts--range) "___return((TSRange *)ts_parser_included_ranges);"))
(define ts--parser-parse                           (c-lambda ((pointer ts--parser) (pointer ts--tree)) (pointer ts--tree) "TSTree * t = ts_parser_parse(___arg1, ___arg2, ip); ___return(t);"))
(define ts--parser-parse-string                    (c-lambda ((pointer ts--parser) (pointer ts--tree) nonnull-UTF-8-string int) (pointer ts--tree) "ts_parser_parse_string"))
(define ts--parser-parse-string-with-encoding      (c-lambda ((pointer ts--parser) (pointer ts--tree) nonnull-UTF-8-string int int) (pointer ts--tree) "ts_parser_parse_string_encoding"))
(define ts--parser-position-rewind!                (c-lambda ((pointer ts--parser)) void "ts_parser_reset"))
(define ts--parser-timeout?                        (c-lambda ((pointer ts--parser)) unsigned-int64 "ts_parser_timeout_micros"))
(define ts--parser-timeout-set!                    (c-lambda ((pointer ts--parser) unsigned-int64) void "ts_parser_set_timeout_micros"))
(define ts--parser-cancellation-set!               (c-lambda ((pointer ts--parser) (pointer size_t)) void "ts_parser_set_cancellation_flag"))
(define ts--parser-cancellation?                   (c-lambda ((pointer ts--parser)) (pointer size_t) "___return((size_t *)ts_parser_cancellation_flag);"))

(define ts--tree-copy!                             (c-lambda ((pointer ts--tree)) (pointer ts--tree) "ts_tree_copy"))
(define ts--tree-free!                             (c-lambda ((pointer ts--tree)) void "ts_tree_delete"))
(define ts--tree-root-node                         (c-lambda ((pointer ts--tree)) ts--node "ts_tree_root_node"))
(define ts--tree-language?                         (c-lambda ((pointer ts--tree)) (pointer ts--language) "___return((TSLanguage *)ts_tree_language);"))
(define ts--tree-edit!                             (c-lambda ((pointer ts--tree) (pointer ts--input-edit)) void "ts_tree_edit"))
(define ts--tree-get-changed-ranges?               (c-lambda ((pointer ts--tree) (pointer ts--tree) (pointer unsigned-int32)) (pointer ts--range) "ts_tree_get_changed_ranges"))
(define ts--tree-write                             (c-lambda ((pointer ts--tree) (pointer "FILE")) void "ts_tree_print_dot_graph"))

(define ts--node-type                              (c-lambda (ts--node) scheme-object
                                                             "___SCMOBJ o;
                                                             char * t = (char *) ts_node_type(___arg1);
                                                             ___UTF_8STRING_to_SCMOBJ(___ps,t,&o,___STILL);
                                                             ___return(o);"))
(define ts--node-symbol                            (c-lambda (ts--node) ts--symbol "ts_node_symbol"))

;; position
(define ts--node-start-byte                        (c-lambda (ts--node) unsigned-int32 "ts_node_start_byte"))
(define ts--node-start-point                       (c-lambda (ts--node) ts--point "ts_node_start_point"))
(define ts--node-end-byte                          (c-lambda (ts--node) unsigned-int32 "ts_node_end_byte"))
(define ts--node-end-point                         (c-lambda (ts--node) ts--point "ts_node_end_point"))

;; the string returned is not be freed afterwards. Don't use this method.
(define ts--node-sexp!                             (c-lambda (ts--node) nonnull-UTF-8-string "char * s = ts_node_string(___arg1); ___return(s);
                                                            #define ___AT_END if (s != NULL) free (s);"))

;; checks
(define ts--node?                                  (c-lambda (ts--node) bool "ts_node_is_null"))
(define ts--node-named?                            (c-lambda (ts--node) bool "ts_node_is_named"))
(define ts--node-missing?                          (c-lambda (ts--node) bool "ts_node_is_missing"))
(define ts--node-extra?                            (c-lambda (ts--node) bool "ts_node_is_extra"))
(define ts--node-has-changes?                      (c-lambda (ts--node) bool "ts_node_has_changes"))
(define ts--node-error?                            (c-lambda (ts--node) bool "ts_node_has_error"))

(define ts--node-parent                            (c-lambda (ts--node) ts--node "ts_node_parent"))
(define ts--node-child-at                          (c-lambda (ts--node unsigned-int32) ts--node "ts_node_named_child"))
(define ts--node-unnamed-child-at                  (c-lambda (ts--node unsigned-int32) ts--node "ts_node_child"))
(define ts--node-field-name-for-child              (c-lambda (ts--node unsigned-int32) nonnull-UTF-8-string "___return((char *)ts_node_field_name_for_child);"))
(define ts--node-child-count                       (c-lambda (ts--node) unsigned-int32 "ts_node_named_child_count"))
(define ts--node-unnamed-child-count               (c-lambda (ts--node) unsigned-int32 "ts_node_child_count"))
(define ts--node-child-by-field                    (c-lambda (ts--node nonnull-UTF-8-string unsigned-int32) ts--node "ts_node_child_by_field_name"))
(define ts--node-child-by-id                       (c-lambda (ts--node ts--field-id) ts--node "ts_node_child_by_field_id"))
(define ts--node-sibling-next                      (c-lambda (ts--node) ts--node "ts_node_next_named_sibling"))
(define ts--node-unnamed-sibling-next              (c-lambda (ts--node) ts--node "ts_node_next_sibling"))
(define ts--node-sibling-prev                      (c-lambda (ts--node) ts--node "ts_node_prev_named_sibling"))
(define ts--node-unnamed-sibling-prev              (c-lambda (ts--node) ts--node "ts_node_prev_sibling"))
(define ts--node-unnamed-first-child-for-byte      (c-lambda (ts--node unsigned-int32) ts--node "ts_node_first_child_for_byte"))
(define ts--node-first-child-for-byte              (c-lambda (ts--node unsigned-int32) ts--node "ts_node_first_named_child_for_byte"))
(define ts--node-descendant-for-byte-range         (c-lambda (ts--node unsigned-int32 unsigned-int32) ts--node "ts_node_named_descendant_for_byte_range"))
(define ts--node-unnamed-descendant-for-byte-range (c-lambda (ts--node unsigned-int32 unsigned-int32) ts--node "ts_node_descendant_for_byte_range"))
(define ts--node-edit!                             (c-lambda ((pointer ts--node) (pointer ts--input-edit)) void "ts_node_edit"))
(define ts--node-equals?                           (c-lambda (ts--node ts--node) bool "ts_node_eq"))

(define ts--cursor-new                             (c-lambda (ts--node) ts--cursor "ts_tree_cursor_new"))
(define ts--cursor-free!                           (c-lambda ((pointer ts--cursor)) void "ts_tree_cursor_delete"))
(define ts--cursor-seek!                           (c-lambda ((pointer ts--cursor) ts--node) void "ts_tree_cursor_reset"))
(define ts--cursor-current-node                    (c-lambda ((pointer ts--cursor)) ts--node "ts_tree_cursor_current_node"))
(define ts--cursor-current-node-field-name         (c-lambda ((pointer ts--cursor)) scheme-object
                                                             "___SCMOBJ obj;
                                                             char * tmp = (char *) ts_tree_cursor_current_field_name(___arg1);
                                                             ___UTF_8STRING_to_SCMOBJ(___ps, tmp, &obj, ___STILL);
                                                             ___return(obj);"))
(define ts--cursor-current-node-field-id           (c-lambda ((pointer ts--cursor)) ts--field-id "ts_tree_cursor_current_field_id"))
(define ts--cursor-parent!                         (c-lambda ((pointer ts--cursor)) bool "ts_tree_cursor_goto_parent"))
(define ts--cursor-sibling-next!                   (c-lambda ((pointer ts--cursor)) bool "ts_tree_cursor_goto_next_sibling"))
(define ts--cursor-child!                          (c-lambda ((pointer ts--cursor)) bool "ts_tree_cursor_goto_first_child"))
(define ts--cursor-child-index-at-byte!            (c-lambda ((pointer ts--cursor) unsigned-int32) unsigned-int64 "ts_tree_cursor_goto_first_child_for_byte"))
(define ts--cursor-child-index-at-point!           (c-lambda ((pointer ts--cursor) ts--point) unsigned-int64 "ts_tree_cursor_goto_first_child_for_point"))
(define ts--cursor-copy                            (c-lambda ((pointer ts--cursor)) ts--cursor "ts_tree_cursor_copy"))

;; user defined
(define (ts--cursor-move-to-field! cursor field)
  (ts--cursor-seek! cursor
                    (ts--node-child-by-field
                      (ts--cursor-current-node cursor)
                      field
                      (string-length field))))
(define (ts--cursor-current-type cursor)           (ts--node-type (ts--cursor-current-node cursor)))

;; query
(define ts--query-new                              (c-lambda ((pointer ts--language) nonnull-UTF-8-string unsigned-int32 (pointer unsigned-int32) (pointer ts--query-error)) (pointer ts--query) "ts_query_new"))
(define ts--query-free!                            (c-lambda ((pointer ts--query)) void "ts_query_delete"))
(define ts--query-count-pattern                    (c-lambda ((pointer ts--query)) unsigned-int32 "ts_query_pattern_count"))
(define ts--query-count-capture                    (c-lambda ((pointer ts--query)) unsigned-int32 "ts_query_capture_count"))
(define ts--query-count-string                     (c-lambda ((pointer ts--query)) unsigned-int32 "ts_query_string_count"))
(define ts--query-pattern-start-byte               (c-lambda ((pointer ts--query) unsigned-int32) unsigned-int32 "ts_query_start_byte_for_pattern"))

(define ts--predicate-next!                        (c-lambda ((pointer ts--query-predicate-step)) (pointer ts--query-predicate-step) "TSQueryPredicateStep * p = ___arg1; p++; ___result = p;"))
(define ts--query-predicates-for-pattern           (c-lambda ((pointer ts--query) unsigned-int32 (pointer unsigned-int32))
                                                             (pointer ts--query-predicate-step)
"uint32_t len = 0;
TSQueryPredicateStep * step = (TSQueryPredicateStep *) ts_query_predicates_for_pattern(___arg1, ___arg2, &len);
* ___arg3 = len;
___return(step);"))
(define ts--query-is-pattern-guaranteed-at-step    (c-lambda ((pointer ts--query) unsigned-int32) bool "ts_query_is_pattern_guaranteed_at_step"))
(define ts--query-start-byte-for-pattern           (c-lambda ((pointer ts--query) unsigned-int32) unsigned-int32 "ts_query_start_byte_for_pattern"))
(define ts--query-capture-name-for-id              (c-lambda ((pointer ts--query) unsigned-int32)
                                                             nonnull-UTF-8-string
"uint32_t len = 0;
const char * str = ts_query_capture_name_for_id(___arg1, ___arg2, &len);
char * ret = malloc((len * sizeof(char)) +1);
strcpy(ret, str);
___return(ret);
"))
(define ts--query-string-value-for-id              (c-lambda ((pointer ts--query) unsigned-int32)
                                                             nonnull-UTF-8-string
"uint32_t len = 0;
const char * str = ts_query_string_value_for_id(___arg1, ___arg2, &len);
char * ret = malloc((len * sizeof(char)) +1);
strcpy(ret, str);
___return(ret);
"))

(define ts--query-cursor-new                       (c-lambda () (pointer ts--query-cursor) "ts_query_cursor_new"))
(define ts--query-cursor-free!                     (c-lambda ((pointer ts--query-cursor)) void "ts_query_cursor_delete"))
(define ts--query-cursor-execute!                  (c-lambda ((pointer ts--query-cursor) (pointer ts--query) ts--node) void "ts_query_cursor_exec"))
(define ts--query-cursor-did-exceed-limit?         (c-lambda ((pointer ts--query-cursor)) bool "___return(ts_query_cursor_did_exceed_match_limit(___arg1));"))
(define ts--query-cursor-exceed-limit              (c-lambda ((pointer ts--query-cursor)) unsigned-int32 "___return(ts_query_cursor_match_limit(___arg1));"))
(define ts--query-cursor-match-limit-set!          (c-lambda ((pointer ts--query-cursor) unsigned-int32) void "ts_query_cursor_set_match_limit;"))
(define ts--query-cursor-limit-execution-range!    (c-lambda ((pointer ts--query-cursor) unsigned-int32 unsigned-int32) void "ts_query_cursor_set_byte_range(___arg1, ___arg2, ___arg3);"))


(define ts--query-cursor-next-match!               (c-lambda ((pointer ts--query-cursor) (pointer ts--query-match)) bool "ts_query_cursor_next_match"))
(define ts--query-cursor-remove-match!             (c-lambda ((pointer ts--query-cursor) unsigned-int32) void "ts_query_cursor_remove_match"))
(define ts--query-cursor-next-capture!             (c-lambda ((pointer ts--query-cursor) (pointer ts--query-match) (pointer unsigned-int32)) bool "ts_query_cursor_next_capture"))

(define ts--lang-symbol-count                      (c-lambda ((pointer ts--language)) unsigned-int32 "ts_language_symbol_count"))
(define ts--lang-symbol->name                      (c-lambda ((pointer ts--language) ts--symbol) scheme-object
                                                             "___SCMOBJ obj;
                                                             char * tmp = (char *) ts_language_symbol_name(___arg1, ___arg2);
                                                             ___UTF_8STRING_to_SCMOBJ(___ps, tmp,&obj,___STILL);
                                                             ___return(obj);"))
(define ts--lang-name->symbol                      (c-lambda ((pointer ts--language) nonnull-UTF-8-string unsigned-int32 bool) ts--symbol "ts_language_symbol_for_name"))
(define ts--lang-field-names-count                 (c-lambda ((pointer ts--language)) unsigned-int32 "ts_language_field_count"))
(define ts--lang-id->field-name                    (c-lambda ((pointer ts--language) ts--field-id) nonnull-UTF-8-string "___return((char *)ts_language_field_name_for_id);"))
(define ts--lang-field-name->id                    (c-lambda ((pointer ts--language) nonnull-UTF-8-string unsigned-int32) ts--field-id "ts_language_field_id_for_name"))
(define ts--lang-symbol-type                       (c-lambda ((pointer ts--language) ts--symbol) ts--symbol-type "ts_language_symbol_type"))
(define ts--lang-version                           (c-lambda ((pointer ts--language)) unsigned-int32 "ts_language_version"))
