# buildfile for Gambit-C Scheme tree-sitter bindings.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-ltree-sitter"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/ts-binding.o1 src/ts-c-ffi-binding.scm
